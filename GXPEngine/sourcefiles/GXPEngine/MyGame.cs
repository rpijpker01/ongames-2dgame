using System;									// System contains a lot of default C# libraries 
using GXPEngine;                                // GXPEngine contains the engine
using System.Collections.Generic;

public class MyGame : Game
{

    private Sprite cutscene6;
    private Sprite cutscene5;
    private Sprite cutscene4;
    private Sprite cutscene3;
    private Sprite cutscene2;
    private Sprite cutscene1;

    private Sprite pressKeyToContinue;
    private Sprite pressKeyToStart;

    private int _currentCutscene = 1;

    public bool cutScenesArePlaying = true;

    public MyGame() : base(1280, 800, false)        // Create a window that's 800x600 and NOT fullscreen
    {
        AutoLevel level0 = new AutoLevel(new System.Drawing.Bitmap("../Sprites/other_sprites/level0.png"));
        level0.scale = (2f / 3f);
        AddChild(level0);
        CutScenes();
    }

    private void Update()
    {
        if (cutScenesArePlaying)
        {
            if (Input.GetKeyDown(Key.SPACE) && _currentCutscene >= 1)
            {
                switch (_currentCutscene)
                {
                    case 7:
                        cutScenesArePlaying = false;
                        RemoveChild(pressKeyToStart);
                        break;
                    case 6:
                        RemoveChild(cutscene6);
                        RemoveChild(pressKeyToContinue);
                        pressKeyToStart = new Sprite("../Sprites/text/press start.png");
                        pressKeyToStart.x = 640 - pressKeyToStart.width / 2;
                        pressKeyToStart.y = 600;
                        AddChild(pressKeyToStart);
                        break;
                    case 5:
                        RemoveChild(cutscene5);
                        break;
                    case 4:
                        RemoveChild(cutscene4);
                        break;
                    case 3:
                        RemoveChild(cutscene3);
                        break;
                    case 2:
                        RemoveChild(cutscene2);
                        break;
                    case 1:
                        RemoveChild(cutscene1);
                        break;
                    default:
                        break;
                }
                _currentCutscene++;
            }
        }
    }

    static void Main()                          // Main() is the first method that's called when the program is run
    {
        new MyGame().Start();                   // Create a "MyGame" and start it
    }

    private void CutScenes()
    {
        cutscene6 = new Sprite("../Sprites/text/6 scene.png");
        cutscene5 = new Sprite("../Sprites/text/5 scene.png");
        cutscene4 = new Sprite("../Sprites/text/4 scene.png");
        cutscene3 = new Sprite("../Sprites/text/3 scene.png");
        cutscene2 = new Sprite("../Sprites/text/2 scene.png");
        cutscene1 = new Sprite("../Sprites/text/first scene.jpg");
        cutscene1.width = 1280;
        cutscene1.height = 800;
        cutscene2.width = 1280;
        cutscene2.height = 800;
        cutscene3.width = 1280;
        cutscene3.height = 800;
        cutscene4.width = 1280;
        cutscene4.height = 800;
        cutscene5.width = 1280;
        cutscene5.height = 800;
        cutscene6.width = 1280;
        cutscene6.height = 800;
        AddChild(cutscene6);
        AddChild(cutscene5);
        AddChild(cutscene4);
        AddChild(cutscene3);
        AddChild(cutscene2);
        AddChild(cutscene1);

        pressKeyToContinue = new Sprite("../Sprites/text/space contiinue.png");
        pressKeyToContinue.scale = 0.5f;
        pressKeyToContinue.x = 1280 - pressKeyToContinue.width;
        pressKeyToContinue.y = 800 - pressKeyToContinue.height;
        AddChild(pressKeyToContinue);
    }
}