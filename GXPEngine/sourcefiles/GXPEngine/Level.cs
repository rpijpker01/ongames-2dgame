﻿using System;
using GXPEngine;
public class Level : Pivot
{
    protected float[] _spawnPoint = new float[2] { 100 , 200};
	private Player _player;

    public Level()
	{
		for (int i = 0; i < 30; i++)
		{
			Ground ground = new Ground();
			ground.x = i * 64;
			ground.y = 704;
			AddChild(ground);
		}

		for (int i = 0; i < 90; i++)
		{
			Ground ground = new Ground();
			ground.y = i * 64;
			ground.x = 900;
			ground.rotation = 90;
			AddChild(ground);
		}

		Ground platform = new Ground();
		platform.x = 600;
		platform.y = 540;
		AddChild(platform);

        Spike Spike = new Spike();
        Spike.x = 680;
        Spike.y = 704 - 64;
        AddChild(Spike);

        BrownDuck duck = new BrownDuck(1);
		duck.x = 128;
		duck.y = 64;
		AddChild(duck);

		Player player = new Player(3);
		_player = player;
		AddChild(player);
	}

    void SpawnPoint()
    {

    }

	void Update()
	{
		if (_player.y <= game.height / 2)
		{
			this.y = -_player.y + game.height / 2;
		}
	}
}
