﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GXPEngine;

class Spike : AnimationSprite
{
    //---Collision Points
    private Sprite[] CollsionPoints = new Sprite[4]
    {
        //- Top
        new Sprite("../Sprites/Collision Extra/point.png"),  
        //- Bottom
        new Sprite("../Sprites/Collision Extra/point.png"), 
        //- Left 
        new Sprite("../Sprites/Collision Extra/point.png"),
        //- Right
        new Sprite("../Sprites/Collision Extra/point.png")
    };

    //---Collision check
    private bool _top = false;
    private bool _bottom = false;
    private bool _left = false;
    private bool _right = false;

    //---timer
    bool _checked = false;

    public Spike() : base("../Sprites/tiles/spike_sheet.png", 4,2,-1)
    {
        SetOrigin(width / 2, height / 2 + 1);
        height += 2;

        SetCollisionPoints();

    }

    void Update()
    {
        if (!_checked && parent != null)
        {
            Collision();
            _checked = true;

            DestroyChildren();
            UpdateSprite();

            //Console.WriteLine(_top + " , " + _bottom + " , " + _left + " , " + _right);
        }
    }

    void SetCollisionPoints()
    {
        //- Top
        CollsionPoints[0].x += 0;
        CollsionPoints[0].y -= height / 2 + 32;

        //- Bottom
        CollsionPoints[1].x += 0;
        CollsionPoints[1].y += height / 2 + 32;

        //- Left
        CollsionPoints[2].x -= width / 2 + 32;
        CollsionPoints[2].y += 0;

        //- Right
        CollsionPoints[3].x += width / 2 + 32;
        CollsionPoints[3].y += 0;

        //Add Collision Points as child
        AddChild(CollsionPoints[0]);
        AddChild(CollsionPoints[1]);
        AddChild(CollsionPoints[2]);
        AddChild(CollsionPoints[3]);

        foreach (Sprite Point in CollsionPoints)
        {
            Point.alpha = 0;
        }
    }
    void Collision()
    {
        float CheckRange = 96f;
        double DistanceAway;

        List<GameObject> Objects = parent.GetChildren();
        foreach (GameObject Obj in Objects)
        {
            DistanceAway = CalDistance(Obj.x, Obj.y, (float)this.x, (float)this.y);
            if (DistanceAway <= CheckRange)
            {
                for (int iterator = 0; iterator < CollsionPoints.Length; iterator++)
                {
                    if (CollsionPoints[iterator].HitTest(Obj))
                    {
                        if (Obj is Spike)
                        {
                            switch (iterator)
                            {
                                //- Top
                                case 0:
                                    _top = true;
                                    break;
                                case 1:
                                    //- Bottom
                                    _bottom = true;
                                    break;
                                case 2:
                                    //- Left
                                    _left = true;
                                    break;
                                case 3:
                                    //- Right
                                    _right = true;
                                    break;
                            }
                        }
                    }
                }
            }
        }
    }
    double CalDistance(float x1, float y1, float x2, float y2)
    {
        float XDif = x1 - x2;
        float YDif = y1 - y2;

        double dis = Math.Pow(XDif, 2) + Math.Pow(YDif, 2);
        dis = Mathf.Sqrt((float)dis);

        return dis;
    }

    void DestroyChildren()
    {
        foreach (Sprite Point in CollsionPoints)
        {
            Point.Destroy();
        }
    }
    void UpdateSprite()
    {
        //----- Singles
        //- Non around
        if (!_top && !_bottom && !_left && !_right)
        {
            SetFrame(0);
        }

        //----- Bars
        //- Left & Right
        else if (_left && _right)
        {
            SetFrame(2);
        }

        //- Left
        else if (_right)
        {
            SetFrame(1);
        }
        //- Right
        else if (_left)
        {
            SetFrame(3);
        }

    }

}