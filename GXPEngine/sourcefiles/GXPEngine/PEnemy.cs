﻿using System;
using System.Collections.Generic;
using GXPEngine;
public class PEnemy : GameObject
{
	public AnimationSprite DuckSprite
    {
        get;
        set;
    }
	protected float _xSpeed;
	protected float _ySpeed;

	protected bool _wallLeft = true;
	protected bool _wallRight;
	protected bool _wallTop  ;
    protected bool _grounded ;

	private bool _playerGotInRange;

	public PEnemy(AnimationSprite sprite)
	{
        DuckSprite = sprite;
        DuckSprite.SetOrigin(DuckSprite.width / 2, DuckSprite.height / 2);
    }

	private void Update()
	{
	}

	protected void UpdateMovement(List<GameObject> objects)
	{
        if (_playerGotInRange == false)
        {
            Player player = null;
            foreach (Object obj in objects)
            {
                if (obj is Player)
                {
                    player = obj as Player;
                }
            }
            if (player != null)
            {
                if (player.y < y + game.height + 800)
                {
                    _playerGotInRange = true;
                }
            }
        }

        if (_playerGotInRange == true)
        {
            x += _xSpeed;
            Collision(true, objects);
            y += _ySpeed;
            Collision(false, objects);
        }
    }

	private void Collision(bool horizontal, List<GameObject> objects)
	{
		float CheckRange = 150f;

		if (objects != null)
		{
			foreach (GameObject Obj in objects)
			{
				if (DistanceTo(Obj) <= CheckRange)
				{

                    float yOffset = 0.1f;

					if (!(Obj is PEnemy) && (Obj is Ground) )
					{

						if (DuckSprite.HitTest(Obj))
						{

                            Ground ObjT = Obj as Ground;

                            if (!horizontal)
                            {
                                //top
                                if (y - (DuckSprite.height / 2 - (8)) > Obj.y)
                                {
                                    y = Obj.y + (64 + DuckSprite.height / 2);
                                    _wallTop = true;

                                }
                                //bottom
                                if (y + (DuckSprite.height / 2 - (8)) < Obj.y)
                                {
                                    y = ObjT.y - (DuckSprite.height / 2 + ObjT.height / 2) - yOffset;
                                    _grounded = true;
                                    _ySpeed = 0;
                                }
                            }

                            if (horizontal)
							{
                                //right
                                if (x + (DuckSprite.width / 2) < Obj.x)
                                {
                                    x = Obj.x - (32 + DuckSprite.width / 2);
                                    _wallLeft = true;
                                }
                                //left
                                if (x - (DuckSprite.width / 2) > Obj.x)
                                {
                                    x = Obj.x + (32 + DuckSprite.width / 2);
                                    _wallRight = true;
                                }
                            }
						}
					}

                    if (!(Obj is PEnemy) && (Obj is Spike))
                    {

                        if (DuckSprite.HitTest(Obj))
                        {

                            Spike ObjT = Obj as Spike;

                            if (!horizontal)
                            {
                                //top
                                if (y - (DuckSprite.height / 2 - (8)) > Obj.y)
                                {
                                    y = Obj.y + (64 + DuckSprite.height / 2);
                                    _wallTop = true;

                                }
                                //bottom
                                if (y + (DuckSprite.height / 2 - (8)) < Obj.y)
                                {
                                    y = ObjT.y - (DuckSprite.height / 2 + ObjT.height / 2) - yOffset;
                                    _grounded = true;
                                    _ySpeed = 0;
                                }
                            }

                            if (horizontal)
                            {
                                //right
                                if (x + (DuckSprite.width / 2) < Obj.x)
                                {
                                    x = Obj.x - (32 + DuckSprite.width / 2);
                                    _wallLeft = true;
                                }
                                //left
                                if (x - (DuckSprite.width / 2) > Obj.x)
                                {
                                    x = Obj.x + (32 + DuckSprite.width / 2);
                                    _wallRight = true;
                                }
                            }
                        }
                    }
                }
			}
		}
	}

	private double CalDistance(float x1, float y1, float x2, float y2)
	{
		float XDif = x1 - x2;
		float YDif = y1 - y2;

		double dis = Math.Pow(XDif, 2) + Math.Pow(YDif, 2);
		dis = Mathf.Sqrt((float)dis);

		return dis;
	}
}
