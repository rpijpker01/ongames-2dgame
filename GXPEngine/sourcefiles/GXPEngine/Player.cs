﻿using System;
using GXPEngine;
using System.Collections.Generic;
public class Player : AnimationSprite
{
    //---List for all objects in layer 4 of level
    private List<GameObject> _objects;

    //---Health
    public int Health
    {
        get;
        set;
    }

    //Immunity timer
    //- 60fps
    private int  _immunityTimer    = 30   ;
    private int  _immunityIterator = 0    ;
    private bool _immune           = false;

    //KnockBack
    private float _damagedKnockbackSpeedX = 8f  * 1.5f;
    private float _damagedKnockbackSpeedY = 12f * 1.5f;

    //---Movement
    private float _xMaxSpeed = 8f ;
    private float _yMaxSpeed = 16f;
    private float _xSpeed         ;
	private float _ySpeed         ;

    private int _direction;

    //---Collision
    private bool _grounded ;
    private bool _wallLeft ;
    private bool _wallRight;
    private bool _wallTop  ;

    //---Jumping
    private bool _doubleJump = false;

    //---Parent Check
    private bool _parentlevel = false;
    private int _colorValue = 255;
    private int _colorResetMultiplier = 8;

    //---Carrot Points
    public int Points { get; set; }

    //---Timer for updating the animation of the rabbit
    private int _animationTimer;

    public Player(int _StartingHealth, float _X = 100, float _Y = 100 ) : base("../Sprites/characters/rabbit_sheet.png", 4, 2, 8)
	{
        SetOrigin(width / 2, height / 2);

        x = _X;
        y = _Y;

        height = 64;
		width = 64;
        Health = _StartingHealth;

        SetFrame(7);
	}

	private void Update()
	{
        if (!isParentPaused())
        {
            GetObjects();

            if (_objects != null)
            {
                //Collision();
                Movement();
                CarrotCollision();

                //Dammaged

                EnemyCollision();
                ExitCollision();
                ImmunityCounter();
                Annihilation();
                ColorReset();
            }
        }
	}

    private void GetObjects()
    {
        if(parent != null)
        {
            _objects = parent.GetChildren();
        }
    }

    private void Movement()
    {
        VerticalMovement();
        HorizontalMovement();
        Falling();
        UpdateMovement();

        Resistance();

        Animations();
	}

	private void HorizontalMovement()
	{
        
		if (Input.GetKey(Key.A) || Input.GetKey(Key.D))
		{
            
            if (Input.GetKey(Key.A) && _xSpeed > -_xMaxSpeed)
            // && !_wallLeft
            {
                _wallRight = false;
                _grounded = false;
                _xSpeed -= 1.5f;

                _direction = Math.Sign(_xSpeed);
            }
			if (Input.GetKey(Key.D) && _xSpeed < _xMaxSpeed)
            // && !_wallRight
            {
                _wallLeft = false;
                _grounded = false;
                _xSpeed += 1.5f;

                _direction = Math.Sign(_xSpeed);

            }
		}
		else
		{
            if (_xSpeed< 1f && _xSpeed > -1f)
			{
				_xSpeed = 0f;
			}
		}
		
	}

	private void VerticalMovement()
	{
		if (Input.GetKeyDown(Key.SPACE) || Input.GetKeyDown(Key.W) || Input.GetKeyDown(Key.UP))
        {
            if (!_grounded && _doubleJump) 
             {
                _ySpeed = -_yMaxSpeed;
                _doubleJump = false;
                _grounded = false;
                DoubleJumpPoof poof = new DoubleJumpPoof();
                poof.x = x;
                poof.y = y + height / 2;
                if(parent != null)
                {
                    if(parent.parent != null)
                    {
                        (parent.parent as AutoLevel).layer5.AddChild(poof);
                    }
                }
            }
        }

		if (Input.GetKey(Key.SPACE) || Input.GetKey(Key.W) || Input.GetKey(Key.UP))
        {
            if (_grounded)
            {
                _ySpeed = -_yMaxSpeed;
                _grounded = false;
            }
        }

        if (Input.GetKey(Key.S) || Input.GetKey(Key.DOWN))
        {
            if (!_grounded)
            {
                _ySpeed += 1;
            }
        }
    }

    private void Falling()
    {
        if (!_grounded)
        {

            if (Math.Sign(_ySpeed) == -1)
            {
                if (_ySpeed > -0.8)
                {
                    _ySpeed = 1.5f;
                }
                else
                {
                    _ySpeed /= 1.1f;
                }
            }
            else
            {
                if (_ySpeed < 13f)
                {
                    if (_ySpeed == 0f)
                    {
                        _ySpeed = 1f;
                    }
                    _ySpeed *= 1.1f;
                }
            }
        }
        else
        {
            if (_ySpeed > 0)
            {
                _ySpeed = 0f;
            }
        }
        
    }

    private void UpdateMovement()
    {
        x += _xSpeed;
        CollisionGround(true);
        y += _ySpeed;
        CollisionGround(false);
    }

	private void Resistance()
	{
		if (_grounded)
		{
			if (_xSpeed > 0f)
			{
				_xSpeed -= 1f;
			}
			if (_xSpeed < 0f)
			{
				_xSpeed += 1f;
			}
		}
		else
		{
			if (_xSpeed > 0f)
			{
				_xSpeed -= 0.3f;
			}
			if (_xSpeed < 0f)
			{
				_xSpeed += 0.3f;
			}
		}
	}   

	private void Animations()
	{
        if (_ySpeed < -1)
        {
            SetFrame(3);
        }
        else if (_ySpeed >= -1 && _ySpeed <= 1 && _ySpeed != 0)
        {
            SetFrame(4);
        }
        else if (_ySpeed > 1)
        {
            SetFrame(5);
        }
        else if (Input.GetKey(Key.A) || Input.GetKey(Key.LEFT) || Input.GetKey(Key.D) || Input.GetKey(Key.RIGHT))
        {
            
            if (_animationTimer == 0)
            {
                _animationTimer = 2;
                if (currentFrame == 6)
                {
                    SetFrame(0);
                }
                else
                {
                    NextFrame();
                }
                
            }
            else
            {
                _animationTimer--;
            }
            
        }
        else
        {
            SetFrame(7);
        }

        if (Input.GetKey(Key.A) || Input.GetKey(Key.LEFT))
        {
            Mirror(true, false);
        }
        if (Input.GetKey(Key.D) || Input.GetKey(Key.RIGHT))
        {
            Mirror(false, false);
        }
    }

    private void CollisionGround(bool horizontal)
    {
        float CheckRange = 150f;
        foreach (GameObject obj in _objects)
        {
            if (DistanceTo(obj) <= CheckRange && HitTest(obj))
            {
                if (obj is Ground)
                {
                    if (!horizontal)
                    {
                        Ground ObjT = obj as Ground;
                        //top
                        if (this.y - (height / 2) > obj.y)
                        {
                            y = obj.y + (height / 2 + ObjT.height / 2);
                            _ySpeed = 0;
                            _wallTop = true;

                        }
                        //bottom

                        if (this.y + (height / 2) < obj.y)
                        {
                            y = ObjT.y - (height / 2 + ObjT.height / 2);
                            _grounded = true;
                            _doubleJump = true;
                            _ySpeed = 0;
                        }
                    }

                    if (horizontal)
                    {
                        Ground ObjT = obj as Ground;
                        //left
                        if (this.x + (width / 2) < obj.x)
                        {
                            this.x = obj.x - (ObjT.width / 2 + width / 2);
                            _wallLeft = true;
                            if (_xSpeed > 0)
                            {
                                _xSpeed = 0;
                            }
                        }
                        //right
                        if (this.x - (width / 2) > obj.x)
                        {
                            this.x = obj.x + (ObjT.width / 2 + width / 2);
                            _wallRight = true;
                            if (_xSpeed < 0)
                            {
                                _xSpeed = 0;
                            }
                        }
                    }
                }

                //Spike Collision
                if (obj is Spike)
                {
                    if (!horizontal)
                    {
                        Spike ObjT = obj as Spike;
                        if (this.y - (height / 2) > obj.y)

                        {
                            y = obj.y + (height / 2 + ObjT.height / 2);
                            _ySpeed = 0;
                            _wallTop = true;

                        }
                        //bottom

                        if (this.y + (height / 2) < obj.y)
                        {
                            y = ObjT.y - (this.height / 2 + ObjT.height / 2) - 2;
                            _ySpeed = 0;
                            _doubleJump = true;

                            TouchEnemy();
                        }
                    }

                    if (horizontal)
                    {
                        Spike ObjT = obj as Spike;
                        //left
                        if (this.x + (width / 2) < obj.x)
                        {
                            this.x = obj.x - (ObjT.width / 2 + width / 2);
                            _wallLeft = true;
                            if (_xSpeed > 0)
                            {
                                _xSpeed = 0;
                            }

                            TouchEnemy();
                        }
                        //right
                        if (this.x - (width / 2) > obj.x)
                        {
                            this.x = obj.x + (ObjT.width / 2 + width / 2);
                            _wallRight = true;
                            if (_xSpeed < 0)
                            {
                                _xSpeed = 0;
                            }

                            TouchEnemy();
                        }
                    }
                }
            }
        } 
    }

    private void ImmunityCounter()
    {
        //immunity countdown
        if (_immune)
        {
            if (_immunityIterator == 0)
            {
                _immune = false;
            }

            if (_immunityIterator != 0)
            {
                _immunityIterator--;
            }
        }

        //initiat iterator to equal max value
        if (_immunityIterator <= 0 && !_immune)
        {
            _immunityIterator = _immunityTimer;
        }

    }

    private void Annihilation()
    {
        if (Health <= 0)
        {
            List<GameObject> Objects = game.GetChildren();
            foreach (GameObject Obj in Objects)
            {
                if (Obj is PLevel)
                {
                    PLevel ObjT = Obj as PLevel;
                    ObjT.Points = Points;
                    break;
                }
            }
            
            Destroy();
        }
    }

    private void Knockback()
    {
        _xSpeed += _damagedKnockbackSpeedX * -_direction;
        _ySpeed += -_damagedKnockbackSpeedY;
        _grounded = false;
    }

    private void TouchEnemy()
    {
        if (!_immune)
        {
            SetColor(255, _colorValue / _colorResetMultiplier, _colorValue / _colorResetMultiplier);
            _colorValue = 0;
            Health--;
            Knockback();
        }
        
        _immune = true;
    }

    private void ColorReset()
    {
        
        if (_colorValue != (255 * _colorResetMultiplier))
        {
            SetColor(255, _colorValue / _colorResetMultiplier, _colorValue / _colorResetMultiplier);
            _colorValue++;
        }
    }

    private bool IsParentLevel()
    {
        if (parent.GetType() is PLevel)
        {
            return true;
        }

        return false;
    }

    private double CalDistance(float x1, float y1, float x2, float y2)
    {
        float XDif = x1 - x2;
        float YDif = y1 - y2;

        double dis = Math.Pow(XDif, 2) + Math.Pow(YDif, 2);
        dis = Mathf.Sqrt((float)dis);

        return dis;
    }

    private void CarrotCollision()
    {
        foreach(GameObject obj in _objects)
        {
            if(DistanceTo(obj) < 150 && HitTest(obj))
            {
                if(obj is Carrot)
                {
                    Points++;
                    obj.Destroy();
                    return;
                }
            }
        }
    }

    private void EnemyCollision()
    {
        foreach (GameObject obj in _objects)
        {
            if (obj is PEnemy)
            {
                PEnemy TempEnemy = obj as PEnemy;

                if (DistanceTo(obj) < 150 && HitTest(TempEnemy.DuckSprite))
                {
                    TouchEnemy();
                    return;
                }
            }
        }
    }

    private void ExitCollision()
    {
        foreach (GameObject obj in _objects)
        {
            if (obj is Exit)
            {
                if (DistanceTo(obj) < 150 && HitTest(obj))
                {
                    if(parent != null)
                    {
                        if (parent.parent != null)
                        {
                            (parent.parent as AutoLevel).Paused = true;
                            (parent.parent as AutoLevel).PauseLock = true;
                            return;
                        }
                    }
                }
            }
        }
    }
}

