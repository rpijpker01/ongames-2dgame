﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using GXPEngine;

class CheckPoint : AnimationSprite
{
    public bool CurrentCheckPoint = false;
    private int _animationTimer;

    private bool _flagIsRed = true;

    public CheckPoint() : base("../Sprites/tiles/full_flag_sheet.png", 6, 4, 24)
    {
        SetOrigin(width / 2 , height / 2);
    }

    private void Update()
    {
        Animations();
       
        CollisionWithPlayer();
    }

    private void Animations()
    {
        if (_animationTimer > 0)
        {
            _animationTimer--;
        }
        else
        {
            NextFrame();
            if (_flagIsRed)
            {
                if (currentFrame < 12)
                {
                    SetFrame(12);
                }
            }
            else
            {
                if (currentFrame > 11)
                {
                    SetFrame(0);
                }
            }
            _animationTimer = 7;
        }
    }

    private void CollisionWithPlayer()
    {
        if (_flagIsRed)
        {
            List<GameObject> Objects = game.GetChildren();
            foreach (GameObject Level in Objects)
            {
                if (Level is PLevel)
                {
                    PLevel TempLevel = Level as PLevel;
                    List<GameObject> children = TempLevel.layer4.GetChildren();
                    foreach (GameObject child in children)
                    {
                        if (child is Player)    
                        {
                            if (HitTest(child))
                            {

                                //go through checkpoints
                                foreach (GameObject checkPoints in children)
                                {
                                    if (checkPoints is CheckPoint)
                                    {
                                        CheckPoint TempCheckPoint = checkPoints as CheckPoint;
                                        TempCheckPoint.CurrentCheckPoint = false;
                                    }
                                }

                                TempLevel.CheckPointReached = true;
                                SetFrame(currentFrame + 11);
                                _flagIsRed = false;
                                CurrentCheckPoint = true;
                            }
                        }
                    }
                }
            }
        }
    }
}

