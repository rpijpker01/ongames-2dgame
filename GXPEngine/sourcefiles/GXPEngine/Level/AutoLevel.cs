﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

using GXPEngine;

class AutoLevel : PLevel
{
    Bitmap _levelMap;
    private int _timer = 2;

    private bool _phiMatrixOn;

    private bool _startScreen;

    private Sprite _startscreen = new Sprite("../Sprites/HUDelements/startscreen.png");
    private Sprite _pauseScreen = new Sprite("../Sprites/text/Credits.png");
    private Sprite _pauseScreenContinue = new Sprite("../Sprites/text/space contiinue.png");
    private Sprite _endScreen = new Sprite("../Sprites/text/end.png");
    private Sprite _endScreenTotalScore = new Sprite("../Sprites/text/total score.png");
    private Sprite _endScreenScore = new Sprite("../Sprites/text/score.png");
    private Sprite _endScreenCarrots = new Sprite("../Sprites/text/carrots.png");

    private Canvas _endScreenCanvas = new Canvas(1920, 1920);
    Font font = new Font(FontFamily.GenericSansSerif, 50);

    public AutoLevel(Bitmap level)
    {
        ScreenPositions();
        


        _levelMap = level;
        Setup();
        StartScreen();
    }

    private void ScreenPositions()
    {
        _pauseScreen.SetOrigin(_pauseScreen.width / 2, _pauseScreen.height / 2);
        _pauseScreen.x = 1920 / 2;
        _pauseScreen.y = 1080 / 2;

        _pauseScreenContinue.SetOrigin(_pauseScreenContinue.width / 2, _pauseScreenContinue.height / 2);
        _pauseScreenContinue.x = 1920 / 2;
        _pauseScreenContinue.y = 1080 / 2 + 200;

        _endScreen.SetOrigin(_endScreen.width / 2, _endScreen.height / 2);
        _endScreen.x = 1920 / 2;
        _endScreen.y = 1080 / 2;

        _endScreenScore.x = 1920 / 2 - _endScreenCarrots.width - 200;
        _endScreenScore.y = 1080 / 2 + 160;

        _endScreenCarrots.x = 1920 / 2 - _endScreenCarrots.width - 200;
        _endScreenCarrots.y = 1080 / 2 + 320;

        _endScreenTotalScore.x = 1920 / 2 - _endScreenCarrots.width - 200;
        _endScreenTotalScore.y = 1080 / 2 + 480;
    }

    private void Update()
    {
        if (!Paused)
        {
            MoveScreen();
            Respawn();

            ScoreOverTime();
            if (_timer > 0)
            {
                _timer--;
            }
            if (_timer == 1)
            {
                SetupScreen();
            }
        }
        if (!PauseLock)
        {
            if (Input.GetKeyDown(Key.SPACE))
            {
                PauseScreen();
            }
        }
        if (Input.GetKeyDown(Key.SPACE) && _startScreen && !(parent as MyGame).cutScenesArePlaying)
        {
            SetupScreen();
            _startScreen = false;
            PauseScreen();
            PauseLock = false;
            layer7.RemoveChild(_startscreen);
            _startscreen.Destroy();
        }
        PhiMatrix();
        MoveCurtains();

        if (PauseLock && !_startScreen)
        {
            //--- End screen
            layer7.AddChild(_endScreen);
            layer7.AddChild(_endScreenTotalScore);
            layer7.AddChild(_endScreenScore);
            layer7.AddChild(_endScreenCarrots);

            EndScreenCanvas();
            layer7.AddChild(_endScreenCanvas);
        }

        //CutScenes();
    }

    void Setup()
    {
        // Lock the bitmap's bits.  
        Rectangle rect = new Rectangle(0, 0, _levelMap.Width, _levelMap.Height);
        BitmapData bmpData = _levelMap.LockBits(rect, ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

        // Get the address of the first line.
        IntPtr ptr = bmpData.Scan0;

        // Declare an array to hold the bytes of the bitmap.
        int bytes = bmpData.Stride * _levelMap.Height;
        byte[] rgbValues = new byte[bytes];
        byte[] r = new byte[bytes / 3];
        byte[] g = new byte[bytes / 3];
        byte[] b = new byte[bytes / 3];

        // Copy the RGB values into the array.
        Marshal.Copy(ptr, rgbValues, 0, bytes);

        int count = 0;
        int stride = bmpData.Stride;

        for (int column = 0; column < bmpData.Height; column++)
        {
            for (int row = 0; row < bmpData.Width; row++)
            {
                b[count] = (byte)(rgbValues[(column * stride) + (row * 3)]);
                g[count] = (byte)(rgbValues[(column * stride) + (row * 3) + 1]);
                r[count++] = (byte)(rgbValues[(column * stride) + (row * 3) + 2]);
            }
        }

        GenerateMap(r, g, b, bmpData.Width, bmpData.Height);
    }

    void EndScreenCanvas()
    {
        _endScreenCanvas.graphics.Clear(Color.Transparent);
        _endScreenCanvas.graphics.DrawString(Convert.ToString(Score), font, Brushes.Black, 1920 / 2 - 80, 1080 / 2 + 180);
        _endScreenCanvas.graphics.DrawString(Convert.ToString(_player.Points), font, Brushes.Black, 1920 / 2 - 80, 1080 / 2 + 340);
        _endScreenCanvas.graphics.DrawString(Convert.ToString(Score + (_player.Points * 50)), font, Brushes.Black, 1920 / 2 - 80, 1080 / 2 + 500);
    }

    void GenerateMap(byte[] R, byte[] G, byte[] B, int width, int height)
    {
        //---GenerateMap
        //- Data
        /*
          - Empty               (255 , 255 , 255)
          - Plain Tile          (0   , 0   , 0  ) x
          - Grate Tile          (0   , 0   , 255) 
          - Spike Trap          (255 , 0   , 0  ) ^
          - BrownDuck Enemy     (150 , 0   , 0  ) '
          - Carrot Pickup       (255 , 255 , 0  ) 
          - Spawn point         (255 , 0   , 255) 
          - Check point         (0   , 255 , 255) 
          - Exit                (0   , 0   ,    )
        */

        //----IDs
        /*
        - 0.- Empty
        - 1.- Plain Tile
        - 2.- Grate Tile
        - 3.- Spike Trap
        - 4.- BrownDuck Enemy
        - 5.- Carrot Pickup
        - 6.- Spawn point
        - 7.- Check point
        - 8.- Exit
        */


        //width = x , height = y

        int[,] level = new int[height, width];

        for (int column = 0; column < height; column++)
        {
            for (int row = 0; row < width; row++)
            {
                //- Current Pixel Color in map
                int ColorIndex = row + (column * width);
                //Console.WriteLine(ColorIndex);
                GameObject obj = null;

                //- Empty 
                if (R[ColorIndex] == 255 &&
                    G[ColorIndex] == 255 &&
                    B[ColorIndex] == 255)
                {
                    level[column, row] = 0;
                }

                //- Plain Tile
                if (R[ColorIndex] == 0   &&
                    G[ColorIndex] == 0   &&
                    B[ColorIndex] == 0  )
                {
                    level[column, row] = 1;
                }

                //- Grate Tile -- no color
                if (R[ColorIndex] == 0 &&
                    G[ColorIndex] == 0 &&
                    B[ColorIndex] == 253)
                {
                    //obj = new Player(3);
                }

                //- Spike Trap 
                if (R[ColorIndex] == 255 &&
                    G[ColorIndex] == 0   &&
                    B[ColorIndex] == 0)
                {
                    level[column, row] = 3;
                }

                //- BrownDuck Enemy
                if (R[ColorIndex] == 150 &&
                    G[ColorIndex] == 0 &&
                    B[ColorIndex] == 0)
                {
                    level[column, row] = 4;

                }

                //- Carrot Pickup
                if (R[ColorIndex] == 255 &&
                    G[ColorIndex] == 255 &&
                    B[ColorIndex] == 0)
                {
                    level[column, row] = 5;
                }

                //- Spawn point
                if (R[ColorIndex] == 255 &&
                    G[ColorIndex] == 0 &&
                    B[ColorIndex] == 255)
                {
                    level[column, row] = 6;
                }
                //- Check point
                if (R[ColorIndex] == 0   &&
                    G[ColorIndex] == 255 &&
                    B[ColorIndex] == 255)
                {
                    level[column, row] = 7;
                }
                //- Exit -- no assigned value
                if (R[ColorIndex] == 1 &&
                    G[ColorIndex] == 255 &&
                    B[ColorIndex] == 255)
                {
                    level[column, row] = 8;
                }
            }
            Exit exit = new Exit();
            exit.x = 950;
            exit.y = 64;
            layer4.AddChild(exit);
        }


        AddTileArray(level);

        HUD hud = new HUD(this);
        layer6.AddChild(hud);
    }

    private void PhiMatrix()
    {
        if (Input.GetKeyDown(Key.G))
        {
            if (_phiMatrixOn)
            {
                _phimatrix.alpha = 0;
                _phiMatrixOn = false;
            }
            else
            {
                _phimatrix.alpha = 1;
                _phiMatrixOn = true;
            }
        }
    }

    private void PauseScreen()
    {
        if (Paused && CurtainsOpening)
        {
            CurtainsOpening = false;
            layer7.AddChild(_pauseScreen);
            layer7.AddChild(_pauseScreenContinue);
        }
        else
        {
            if (Paused)
            {
                CurtainsOpening = true;
                layer7.RemoveChild(_pauseScreen);
                layer7.RemoveChild(_pauseScreenContinue);
            }
            else
            {
                Paused = true;
                layer7.AddChild(_pauseScreen);
                layer7.AddChild(_pauseScreenContinue);
            }
        }
    }

    private void StartScreen()
    {
        layer7.AddChild(_startscreen);
        Paused = true;
        PauseLock = true;
        _startScreen = true;
    }

    private void CutScenes()
    {
    }
}

