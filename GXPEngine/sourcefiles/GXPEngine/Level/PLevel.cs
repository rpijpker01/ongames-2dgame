﻿using System;
using System.Collections.Generic;
using GXPEngine;
public class PLevel : Pivot
{
    //protected float[] _spawnPoint = new float[2] { 100 , 200};
    public Player _player { get; set; }
	protected Sprite background1 = new Sprite("../Sprites/other_sprites/small bg.png");
	protected Sprite background = new Sprite("../Sprites/other_sprites/background5.png");

    //--- Layers
	public Pivot layer0 = new Pivot();
	public Pivot layer1 = new Pivot();
	public Pivot layer2 = new Pivot();
	public Pivot layer3 = new Pivot();
	public Pivot layer4 = new Pivot();
	public Pivot layer5 = new Pivot();
	public Pivot layer6 = new Pivot();
    public Pivot layer7 = new Pivot();

    //--- Vars
    protected bool _playerExists = false;
    private float[] _spawnPoint = new float[2];
    public bool CheckPointReached = false;

    //Points
    private int interval = 12;
    private int clock = 0;
    public int Score { get; set; }

    //Player
    public int Points = 0;

    //Phimatrix
    protected Sprite _phimatrix = new Sprite("../Sprites/other_sprites/phimatrix1.png");

    //Pause variable
    public bool Paused;
    public bool CurtainsOpening;
    public bool PauseLock;

    //Create curtains
    Sprite leftCurtain = new Sprite("../Sprites/other_sprites/left curtain.png");
    Sprite rightCurtain = new Sprite("../Sprites/other_sprites/right curtain.png");

    public PLevel()
	{
        _player = new Player(3);
        Score = 4000;
        AddChild(layer0);
        AddChild(layer1);
		AddChild(layer2);
        AddChild(layer3);
        AddChild(layer4);
        AddChild(layer5);
        AddChild(layer6);
        AddChild(layer7);

        for (int i = 0; i < 5760; i += 64)
        {
            Ground leftwall = new Ground();
            Ground rightwall = new Ground();
            leftwall.x = -32;
            rightwall.x = 1952;
            leftwall.y = i + 32;
            rightwall.y = i + 32;
            layer4.AddChild(leftwall);
            layer4.AddChild(rightwall);
        }
        _player.x = 200;
        _player.y = 5000;

        _phimatrix.width = 1920;
        _phimatrix.height = 1200;
        _phimatrix.alpha = 0;

        layer0.AddChild(background);
        layer1.AddChild(background1);
        Sprite foreground = new Sprite("../Sprites/other_sprites/foreground2.png");
        layer5.AddChild(foreground);

        leftCurtain.x = 960 - leftCurtain.width;
        leftCurtain.height = 1250;
        layer7.AddChild(leftCurtain);

        rightCurtain.x = 960;
        rightCurtain.height = 1250;
        layer7.AddChild(rightCurtain);

        //Grid
        layer7.AddChild(_phimatrix);
    }

    protected void Update()
    {
    }

    protected void AddTileArray(int[,] tilearray)
	{
        for (int i = 0; i < tilearray.GetLength(1); i++)
		{
			for (int j = 0; j < tilearray.GetLength(0); j++)
			{
				GameObject obj = null;
				switch (tilearray[j, i])
				{
					case 0:
						break;
					case 1:
						obj = new Ground();
						break;
					case 2:
						obj = new Grate();
						break;
					case 3:
						obj = new Spike();
                        
						break;
					case 4:
						obj = new BrownDuck(1);
						break;
                    case 5:
                        obj = new Carrot();
                        break;
                    case 6:
                        obj = new SpawnPoint();
                        obj.x = -17f;
                        break;
                    case 7:
                        obj = new CheckPoint();
                        obj.x = 10f;
                        break;
                    default:
						break;
				}
				if (obj != null)
				{
					obj.x += i * 64 + 32;
					obj.y += j * 64 + 32;
					layer4.AddChild(obj);
                }
			}
		}
	}

	protected void MoveScreen()
	{
		if (_player.y + _player.height / 2 < 5760 - game.height / 1.38 && _player.y >= (((game.height / 2) * 3) / 2) )
		{
			layer5.y = -_player.y + (((game.height / 2) * 3) / 2);
			layer4.y = -_player.y + (((game.height / 2) * 3) / 2);
			//_layer1.y = -_player.y + (((game.height / 2) * 3) / 2);
			layer1.y = -_player.y / 1.5f + (((game.height / 2) * 3) / 3);
            layer0.y = -_player.y / 1.2f + (((game.height / 2) * 3) / 2.4f);
        }
        if (!(_player.y + _player.height / 2 < 5760 - game.height / 1.38))
        {
            SetupScreen();
        }
    }

    protected void Respawn()
    {
        List<GameObject> Objects = layer4.GetChildren();
        foreach (GameObject Obj in Objects)
        {
            if (Obj is Player)
            {
                _playerExists = true;
            }

            if (Obj is SpawnPoint && !CheckPointReached)
            {
                _spawnPoint[0] = Obj.x;
                _spawnPoint[1] = Obj.y;
            }
            else if (Obj is CheckPoint && CheckPointReached)
            {
                CheckPoint TempObj = Obj as CheckPoint;

                if (TempObj.CurrentCheckPoint)
                {
                    _spawnPoint[0] = Obj.x;
                    _spawnPoint[1] = Obj.y;
                }
            }
        }

        if ( ! _playerExists)
        {
            _player = new Player(3, _spawnPoint[0], _spawnPoint[1]);
            _player.Points = Points;
            layer4.AddChildAt(_player,100000);
        }

        _playerExists = false;
    }

    protected void ScoreOverTime()
    {
        if (clock == interval)
        {
            Score--;
            clock = 0;

            Console.WriteLine(Score);
        }
        clock++;
    }

    protected void SetupScreen()
    {
        layer0.y = -3027.07f;
        layer1.y = -3027.07f;
        layer4.y = -4540.61f;
        layer5.y = -4540.61f;
    }

    protected void MoveCurtains()
    {
        if (Paused && !CurtainsOpening && leftCurtain.x < (960 - leftCurtain.width) + 50)
        {
            leftCurtain.x += 8f;
            rightCurtain.x -= 8f;
        }
        if(CurtainsOpening && leftCurtain.x > (-leftCurtain.width + 120))
        {
            rightCurtain.x += 8f;
            leftCurtain.x -= 8f;
        }
        if (leftCurtain.x <= (-leftCurtain.width + 120))
        {
            Paused = false;
            CurtainsOpening = false;
        }
        
    }

   protected void StartGame()
    {
        if (Input.GetKey(Key.SPACE))
        {
            PauseLock = false;
        }
    }
}
