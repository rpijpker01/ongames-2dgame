﻿using System;
using System.Collections.Generic;
using GXPEngine;
public class BrownDuck : PEnemy
{
	private List<GameObject> _objects = null;
	private int _difficulty;

    private Sprite _cboxbottomright ;
    private Sprite _cboxbottomleft  ;

    private int _animTimer;

    public BrownDuck(int difficulty = 2) : base(new AnimationSprite("../Sprites/characters/duck_sheet.png", 7, 1, 7))
	{
		_difficulty = difficulty;
        DuckSprite.SetOrigin(DuckSprite.width / 2, DuckSprite.height / 2);

        AddCollisionBoxes();

        AddChild(DuckSprite);
	}

	private void Update()
	{
        if (!isParentPaused())
        {
            _objects = parent.GetChildren();
            if (_difficulty != 0)
            {
                FloorCollisions();
                VerticalMovement();
                HorizontalMovement();
            }

            UpdateMovement(_objects);
            Animations();
        }
    }

    private void AddCollisionBoxes()
    {
        _cboxbottomleft  = new Sprite("checkers.png") ;
        _cboxbottomright = new Sprite("checkers.png") ;

        _cboxbottomleft.SetOrigin(_cboxbottomleft.width / 2, _cboxbottomleft.height / 2)    ;
        _cboxbottomright.SetOrigin(_cboxbottomright.width / 2, _cboxbottomright.height / 2) ;

        _cboxbottomleft.height  /= 2 ;
        _cboxbottomright.height /= 2 ;
        _cboxbottomleft.width   /= 2 ;
        _cboxbottomright.width  /= 2 ;

        _cboxbottomleft.y += 64;
        _cboxbottomleft.x -= 50;

        _cboxbottomright.y += 64;
        _cboxbottomright.x += 50;

        _cboxbottomleft.alpha = 0;
        _cboxbottomright.alpha = 0;

        AddChild(_cboxbottomleft);
        AddChild(_cboxbottomright);
    }

    private void FloorCollisions()
    {
        bool floorleft = false;
        bool floorright = false;
        foreach(GameObject obj in _objects)
        {
            if (DistanceTo(obj) < 300)
            {
                if (_cboxbottomleft.HitTest(obj) && (obj is Ground || obj is Spike))
                {
                    floorleft = true;
                }
                if (_cboxbottomright.HitTest(obj) && (obj is Ground || obj is Spike))
                {
                    floorright = true;
                }
            }
        }
        if (!floorleft || !floorright)
        {
            _xSpeed *= -1;
        }
    }

	private void HorizontalMovement()
	{
        if (_wallLeft)
        {
            _xSpeed = -5f;
            _grounded = false;
            _wallLeft = false;

        }
        if (_wallRight)

        {
            _xSpeed = 5f;
            
            _wallRight = false;
        }
        _grounded = false;
    }

	private void VerticalMovement()
	{
		if (!_grounded)
		{

			if (Math.Sign(_ySpeed) == -1)
			{
				if (_ySpeed > -0.8)
				{
					_ySpeed = 1.5f;
				}
				else
				{
					_ySpeed /= 1.1f;
				}
			}
			else
			{
				if (_ySpeed < 6f)
				{
					if (_ySpeed == 0f)
					{
						_ySpeed = 1f;
					}
					_ySpeed *= 1.1f;
				}
			}
		}
		else
		{
			if (_ySpeed > 0)
			{
				_ySpeed = 0f;
			}
		}
	}

	private List<GameObject> GetObjects()
	{
		if (parent is PLevel)
		{
			return parent.GetChildren();
		}
		return null;
	}

    private void Animations()
    {
        if(_xSpeed > 0)
        {
            DuckSprite.Mirror(true, false);
        }
        else
        {
            DuckSprite.Mirror(false, false);
        }
        if(_animTimer > 0)
        {
            _animTimer--;
        }
        if (_animTimer == 0)
        {
            _animTimer = 5;
            DuckSprite.NextFrame();
        }
    }
}