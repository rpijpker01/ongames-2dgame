﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GXPEngine;

class Carrot : AnimationSprite
{
    private int _animTimer;


    public Carrot() : base("../Sprites/tiles/carrot_sheet_new.png", 3, 1, -1)
    {
        SetOrigin(width / 2, height / 2);
    }

    void Update()
    {
        if (_animTimer > 0)
        {
            _animTimer--;
        }
        if (_animTimer == 0)
        {
            NextFrame();
            _animTimer = 8;
        }

    }
}