﻿using System;
using GXPEngine;
public class DoubleJumpPoof : AnimationSprite
{
    private int _animTimer;

	public DoubleJumpPoof() : base("../Sprites/characters/poof.png", 12, 1, 12)
	{
		SetOrigin(width / 2, height / 2);
        Mirror(false, true);
	}

    private void Update()
    {
        if (!(currentFrame == 11))
        {
            if (_animTimer > 0)
            {
                _animTimer--;
            }
            if (_animTimer == 0)
            {
                NextFrame();
                _animTimer = 3;
            }
        }
        else
        {
            this.Destroy();
            return;
        }
    }
}
