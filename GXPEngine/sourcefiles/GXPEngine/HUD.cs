﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

using GXPEngine;

class HUD : Canvas
{
    private int _oldPoints = -1;
    private int _oldHealth = -1;
    public bool isChange = false;

    private PLevel _level;
    private int totalCarrotCount = 0;

    Font font = new Font(FontFamily.GenericSansSerif, 30);

    public HUD(PLevel level) : base(1920, Game.main.height)
    {
        _level = level;
        foreach (GameObject obj in _level.layer4.GetChildren())
        {
            if (obj is Carrot)
            {
                totalCarrotCount++;
            }
        }
    }

    private void Update()
    {
        if (_level.Points != _oldPoints
            || _level._player.Health != _oldHealth)
        {
            _oldHealth = _level._player.Health;
            _oldPoints = _level.Score;
            isChange = true;
        }

        DrawHUD();

    }

    void DrawHUD()
    {
        graphics.Clear(Color.Transparent);

        DrawScore();
        DrawHealthAndCarrots();
    }

    void DrawScore()
    {
        graphics.DrawString(Convert.ToString(_level.Score), font, Brushes.Black, 60, 65);
    }

    void DrawHealthAndCarrots()
    {
        if (isChange)
        {
            for (int i = 0; i < _level._player.Health; i++)
            {
                graphics.DrawImage(Image.FromFile("../Sprites/HUDelements/heart.png"), 1920 - 128 - (i * 68), 50);
            }

            graphics.DrawImage(Image.FromFile("../Sprites/tiles/carrot 1.png"), 1920 / 2 - 90, 30);
            graphics.DrawString(_level._player.Points + " / " + totalCarrotCount, font, Brushes.Black, 1920 / 2 - 10, 65);

            isChange = false;
        }
    }
}
